﻿Imports AForge.Video.DirectShow
Imports AForge.Vision.Motion

Public Class Lista
    Dim dispositivos As FilterInfoCollection
    Dim numDis As Integer

    Private Sub Lista_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        numDis = 0
        dispositivos = New FilterInfoCollection(FilterCategory.VideoInputDevice)
        For Each X As FilterInfo In dispositivos
            CBListaWebcam.Items.Add(X.Name)
            numDis += 1
        Next
        If numDis = 0 Then
            MsgBox("No hay Cámaras Web disponibles.", MsgBoxStyle.Exclamation, "Remote Cursor")
            End
        End If

    End Sub

    Private Sub LIniciar_Click(sender As Object, e As EventArgs) Handles LIniciar.Click
        Form1.LWebCamSelected.Text = CBListaWebcam.SelectedText
        Form1.Enabled = True
        Me.Close()
    End Sub
End Class
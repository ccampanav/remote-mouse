﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.LWebcam = New System.Windows.Forms.Label()
        Me.LAccion = New System.Windows.Forms.Label()
        Me.CBListaWebcam = New System.Windows.Forms.ComboBox()
        Me.LPosX = New System.Windows.Forms.Label()
        Me.TimerCheck = New System.Windows.Forms.Timer(Me.components)
        Me.TimerSquare = New System.Windows.Forms.Timer(Me.components)
        Me.LabelMAXB = New System.Windows.Forms.Label()
        Me.LabelMINB = New System.Windows.Forms.Label()
        Me.LabelMAXG = New System.Windows.Forms.Label()
        Me.LabelMING = New System.Windows.Forms.Label()
        Me.LabelMAXR = New System.Windows.Forms.Label()
        Me.LabelMINR = New System.Windows.Forms.Label()
        Me.TrackBarMAXB = New System.Windows.Forms.TrackBar()
        Me.TrackBarMINB = New System.Windows.Forms.TrackBar()
        Me.TrackBarMAXG = New System.Windows.Forms.TrackBar()
        Me.TrackBarMING = New System.Windows.Forms.TrackBar()
        Me.TrackBarMAXR = New System.Windows.Forms.TrackBar()
        Me.TrackBarMINR = New System.Windows.Forms.TrackBar()
        Me.PBWebCamGray = New System.Windows.Forms.PictureBox()
        Me.PBWebCam = New System.Windows.Forms.PictureBox()
        Me.LObjeto = New System.Windows.Forms.Label()
        Me.LPosY = New System.Windows.Forms.Label()
        Me.LColorEnfocado = New System.Windows.Forms.Label()
        Me.PBColor = New System.Windows.Forms.PictureBox()
        Me.LIluminacion = New System.Windows.Forms.Label()
        CType(Me.TrackBarMAXB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBarMINB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBarMAXG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBarMING, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBarMAXR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBarMINR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBWebCamGray, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBWebCam, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBColor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LWebcam
        '
        Me.LWebcam.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LWebcam.Font = New System.Drawing.Font("Century Gothic", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LWebcam.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LWebcam.Location = New System.Drawing.Point(357, 9)
        Me.LWebcam.Name = "LWebcam"
        Me.LWebcam.Size = New System.Drawing.Size(200, 50)
        Me.LWebcam.TabIndex = 1
        Me.LWebcam.Text = "INICIAR"
        Me.LWebcam.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LAccion
        '
        Me.LAccion.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LAccion.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LAccion.Location = New System.Drawing.Point(16, 437)
        Me.LAccion.Name = "LAccion"
        Me.LAccion.Size = New System.Drawing.Size(330, 20)
        Me.LAccion.TabIndex = 4
        Me.LAccion.Text = "Acción a ejecutar: NINGUNA"
        Me.LAccion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CBListaWebcam
        '
        Me.CBListaWebcam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBListaWebcam.ForeColor = System.Drawing.Color.DimGray
        Me.CBListaWebcam.FormattingEnabled = True
        Me.CBListaWebcam.Location = New System.Drawing.Point(31, 21)
        Me.CBListaWebcam.Name = "CBListaWebcam"
        Me.CBListaWebcam.Size = New System.Drawing.Size(310, 28)
        Me.CBListaWebcam.TabIndex = 7
        '
        'LPosX
        '
        Me.LPosX.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LPosX.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LPosX.Location = New System.Drawing.Point(16, 478)
        Me.LPosX.Name = "LPosX"
        Me.LPosX.Size = New System.Drawing.Size(330, 20)
        Me.LPosX.TabIndex = 8
        Me.LPosX.Text = "Eje X: 0"
        Me.LPosX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TimerCheck
        '
        Me.TimerCheck.Interval = 80
        '
        'TimerSquare
        '
        Me.TimerSquare.Interval = 10
        '
        'LabelMAXB
        '
        Me.LabelMAXB.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMAXB.Location = New System.Drawing.Point(631, 536)
        Me.LabelMAXB.Name = "LabelMAXB"
        Me.LabelMAXB.Size = New System.Drawing.Size(220, 16)
        Me.LabelMAXB.TabIndex = 46
        Me.LabelMAXB.Text = "MaxAzul: 255"
        Me.LabelMAXB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelMINB
        '
        Me.LabelMINB.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMINB.Location = New System.Drawing.Point(378, 536)
        Me.LabelMINB.Name = "LabelMINB"
        Me.LabelMINB.Size = New System.Drawing.Size(220, 16)
        Me.LabelMINB.TabIndex = 45
        Me.LabelMINB.Text = "MinAzul: 0"
        Me.LabelMINB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelMAXG
        '
        Me.LabelMAXG.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMAXG.Location = New System.Drawing.Point(629, 473)
        Me.LabelMAXG.Name = "LabelMAXG"
        Me.LabelMAXG.Size = New System.Drawing.Size(220, 16)
        Me.LabelMAXG.TabIndex = 44
        Me.LabelMAXG.Text = "MaxVerde: 255"
        Me.LabelMAXG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelMING
        '
        Me.LabelMING.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMING.Location = New System.Drawing.Point(379, 473)
        Me.LabelMING.Name = "LabelMING"
        Me.LabelMING.Size = New System.Drawing.Size(220, 16)
        Me.LabelMING.TabIndex = 43
        Me.LabelMING.Text = "MinVerde: 0"
        Me.LabelMING.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelMAXR
        '
        Me.LabelMAXR.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMAXR.Location = New System.Drawing.Point(629, 410)
        Me.LabelMAXR.Name = "LabelMAXR"
        Me.LabelMAXR.Size = New System.Drawing.Size(220, 16)
        Me.LabelMAXR.TabIndex = 42
        Me.LabelMAXR.Text = "MaxRojo: 255"
        Me.LabelMAXR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelMINR
        '
        Me.LabelMINR.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMINR.Location = New System.Drawing.Point(378, 410)
        Me.LabelMINR.Name = "LabelMINR"
        Me.LabelMINR.Size = New System.Drawing.Size(220, 16)
        Me.LabelMINR.TabIndex = 41
        Me.LabelMINR.Text = "MinRojo: 0"
        Me.LabelMINR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TrackBarMAXB
        '
        Me.TrackBarMAXB.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TrackBarMAXB.Location = New System.Drawing.Point(613, 506)
        Me.TrackBarMAXB.Maximum = 255
        Me.TrackBarMAXB.Name = "TrackBarMAXB"
        Me.TrackBarMAXB.Size = New System.Drawing.Size(250, 45)
        Me.TrackBarMAXB.TabIndex = 40
        Me.TrackBarMAXB.Value = 255
        '
        'TrackBarMINB
        '
        Me.TrackBarMINB.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TrackBarMINB.Location = New System.Drawing.Point(363, 506)
        Me.TrackBarMINB.Maximum = 255
        Me.TrackBarMINB.Name = "TrackBarMINB"
        Me.TrackBarMINB.Size = New System.Drawing.Size(250, 45)
        Me.TrackBarMINB.TabIndex = 39
        '
        'TrackBarMAXG
        '
        Me.TrackBarMAXG.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TrackBarMAXG.Location = New System.Drawing.Point(613, 443)
        Me.TrackBarMAXG.Maximum = 255
        Me.TrackBarMAXG.Name = "TrackBarMAXG"
        Me.TrackBarMAXG.Size = New System.Drawing.Size(250, 45)
        Me.TrackBarMAXG.TabIndex = 38
        Me.TrackBarMAXG.Value = 255
        '
        'TrackBarMING
        '
        Me.TrackBarMING.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TrackBarMING.Location = New System.Drawing.Point(363, 443)
        Me.TrackBarMING.Maximum = 255
        Me.TrackBarMING.Name = "TrackBarMING"
        Me.TrackBarMING.Size = New System.Drawing.Size(250, 45)
        Me.TrackBarMING.TabIndex = 37
        '
        'TrackBarMAXR
        '
        Me.TrackBarMAXR.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TrackBarMAXR.Location = New System.Drawing.Point(613, 380)
        Me.TrackBarMAXR.Maximum = 255
        Me.TrackBarMAXR.Name = "TrackBarMAXR"
        Me.TrackBarMAXR.Size = New System.Drawing.Size(250, 45)
        Me.TrackBarMAXR.TabIndex = 36
        Me.TrackBarMAXR.Value = 255
        '
        'TrackBarMINR
        '
        Me.TrackBarMINR.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TrackBarMINR.Location = New System.Drawing.Point(363, 380)
        Me.TrackBarMINR.Maximum = 255
        Me.TrackBarMINR.Name = "TrackBarMINR"
        Me.TrackBarMINR.Size = New System.Drawing.Size(250, 45)
        Me.TrackBarMINR.TabIndex = 35
        '
        'PBWebCamGray
        '
        Me.PBWebCamGray.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PBWebCamGray.Image = Global.RemoteMouse.My.Resources.Resources.webcam
        Me.PBWebCamGray.Location = New System.Drawing.Point(471, 70)
        Me.PBWebCamGray.Name = "PBWebCamGray"
        Me.PBWebCamGray.Size = New System.Drawing.Size(384, 288)
        Me.PBWebCamGray.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBWebCamGray.TabIndex = 11
        Me.PBWebCamGray.TabStop = False
        '
        'PBWebCam
        '
        Me.PBWebCam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PBWebCam.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PBWebCam.Image = Global.RemoteMouse.My.Resources.Resources.webcam
        Me.PBWebCam.Location = New System.Drawing.Point(29, 70)
        Me.PBWebCam.Name = "PBWebCam"
        Me.PBWebCam.Size = New System.Drawing.Size(384, 288)
        Me.PBWebCam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBWebCam.TabIndex = 10
        Me.PBWebCam.TabStop = False
        '
        'LObjeto
        '
        Me.LObjeto.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LObjeto.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LObjeto.Location = New System.Drawing.Point(16, 397)
        Me.LObjeto.Name = "LObjeto"
        Me.LObjeto.Size = New System.Drawing.Size(330, 20)
        Me.LObjeto.TabIndex = 47
        Me.LObjeto.Text = "Objeto detectado: NO"
        Me.LObjeto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LPosY
        '
        Me.LPosY.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LPosY.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LPosY.Location = New System.Drawing.Point(16, 519)
        Me.LPosY.Name = "LPosY"
        Me.LPosY.Size = New System.Drawing.Size(330, 20)
        Me.LPosY.TabIndex = 48
        Me.LPosY.Text = "Eje Y: 0"
        Me.LPosY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LColorEnfocado
        '
        Me.LColorEnfocado.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LColorEnfocado.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LColorEnfocado.Location = New System.Drawing.Point(530, 581)
        Me.LColorEnfocado.Name = "LColorEnfocado"
        Me.LColorEnfocado.Size = New System.Drawing.Size(300, 20)
        Me.LColorEnfocado.TabIndex = 49
        Me.LColorEnfocado.Text = "Color enfocado: NINGUNO"
        Me.LColorEnfocado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PBColor
        '
        Me.PBColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PBColor.Location = New System.Drawing.Point(842, 579)
        Me.PBColor.Name = "PBColor"
        Me.PBColor.Size = New System.Drawing.Size(25, 25)
        Me.PBColor.TabIndex = 50
        Me.PBColor.TabStop = False
        '
        'LIluminacion
        '
        Me.LIluminacion.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LIluminacion.Enabled = False
        Me.LIluminacion.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LIluminacion.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LIluminacion.Location = New System.Drawing.Point(378, 582)
        Me.LIluminacion.Name = "LIluminacion"
        Me.LIluminacion.Size = New System.Drawing.Size(180, 22)
        Me.LIluminacion.TabIndex = 51
        Me.LIluminacion.Text = "Detectar iluminación"
        Me.LIluminacion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(884, 621)
        Me.Controls.Add(Me.LIluminacion)
        Me.Controls.Add(Me.PBColor)
        Me.Controls.Add(Me.LColorEnfocado)
        Me.Controls.Add(Me.LPosY)
        Me.Controls.Add(Me.LObjeto)
        Me.Controls.Add(Me.LabelMAXB)
        Me.Controls.Add(Me.LabelMINB)
        Me.Controls.Add(Me.LabelMAXG)
        Me.Controls.Add(Me.LabelMING)
        Me.Controls.Add(Me.LabelMAXR)
        Me.Controls.Add(Me.LabelMINR)
        Me.Controls.Add(Me.TrackBarMAXB)
        Me.Controls.Add(Me.TrackBarMINB)
        Me.Controls.Add(Me.TrackBarMAXG)
        Me.Controls.Add(Me.TrackBarMING)
        Me.Controls.Add(Me.TrackBarMAXR)
        Me.Controls.Add(Me.TrackBarMINR)
        Me.Controls.Add(Me.PBWebCamGray)
        Me.Controls.Add(Me.PBWebCam)
        Me.Controls.Add(Me.LPosX)
        Me.Controls.Add(Me.CBListaWebcam)
        Me.Controls.Add(Me.LAccion)
        Me.Controls.Add(Me.LWebcam)
        Me.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(900, 660)
        Me.MinimumSize = New System.Drawing.Size(900, 660)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Remote Mouse"
        CType(Me.TrackBarMAXB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBarMINB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBarMAXG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBarMING, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBarMAXR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBarMINR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBWebCamGray, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBWebCam, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBColor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LWebcam As Label
    Friend WithEvents LAccion As Label
    Friend WithEvents CBListaWebcam As ComboBox
    Friend WithEvents LPosX As Label
    Friend WithEvents PBWebCam As PictureBox
    Friend WithEvents TimerCheck As Timer
    Friend WithEvents TimerSquare As Timer
    Friend WithEvents PBWebCamGray As PictureBox
    Friend WithEvents LabelMAXB As Label
    Friend WithEvents LabelMINB As Label
    Friend WithEvents LabelMAXG As Label
    Friend WithEvents LabelMING As Label
    Friend WithEvents LabelMAXR As Label
    Friend WithEvents LabelMINR As Label
    Friend WithEvents TrackBarMAXB As TrackBar
    Friend WithEvents TrackBarMINB As TrackBar
    Friend WithEvents TrackBarMAXG As TrackBar
    Friend WithEvents TrackBarMING As TrackBar
    Friend WithEvents TrackBarMAXR As TrackBar
    Friend WithEvents TrackBarMINR As TrackBar
    Friend WithEvents LObjeto As Label
    Friend WithEvents LPosY As Label
    Friend WithEvents LColorEnfocado As Label
    Friend WithEvents PBColor As PictureBox
    Friend WithEvents LIluminacion As Label
End Class

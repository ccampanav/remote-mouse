﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Lista
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Lista))
        Me.CBListaWebcam = New System.Windows.Forms.ComboBox()
        Me.LIniciar = New System.Windows.Forms.Label()
        Me.LTitle = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'CBListaWebcam
        '
        Me.CBListaWebcam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBListaWebcam.ForeColor = System.Drawing.Color.DimGray
        Me.CBListaWebcam.FormattingEnabled = True
        Me.CBListaWebcam.Location = New System.Drawing.Point(39, 68)
        Me.CBListaWebcam.Name = "CBListaWebcam"
        Me.CBListaWebcam.Size = New System.Drawing.Size(300, 28)
        Me.CBListaWebcam.TabIndex = 0
        '
        'LIniciar
        '
        Me.LIniciar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LIniciar.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LIniciar.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LIniciar.Location = New System.Drawing.Point(95, 118)
        Me.LIniciar.Name = "LIniciar"
        Me.LIniciar.Size = New System.Drawing.Size(200, 44)
        Me.LIniciar.TabIndex = 2
        Me.LIniciar.Text = "INICIAR"
        Me.LIniciar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LTitle
        '
        Me.LTitle.AutoSize = True
        Me.LTitle.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTitle.Location = New System.Drawing.Point(85, 19)
        Me.LTitle.Name = "LTitle"
        Me.LTitle.Size = New System.Drawing.Size(215, 24)
        Me.LTitle.TabIndex = 3
        Me.LTitle.Text = "Escoge la WebCam"
        '
        'Lista
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(384, 181)
        Me.Controls.Add(Me.LTitle)
        Me.Controls.Add(Me.LIniciar)
        Me.Controls.Add(Me.CBListaWebcam)
        Me.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(400, 220)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(400, 220)
        Me.Name = "Lista"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Remote Mouse"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CBListaWebcam As ComboBox
    Friend WithEvents LIniciar As Label
    Friend WithEvents LTitle As Label
End Class

﻿Imports AForge
Imports AForge.Imaging
Imports AForge.Imaging.Filters
Imports AForge.Video
Imports AForge.Video.DirectShow

Public Class Form1
    Private Declare Function SetCursorPos Lib "user32.dll" (ByVal x As Long, ByVal y As Long) As Long 'Mover mouse
    Public Declare Sub mouse_event Lib "user32.dll" (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy _
    As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long) 'Dar click
    Dim dispositivos As FilterInfoCollection
    Dim SOV As VideoCaptureDevice
    Dim iniciar, centinelaClick As Boolean
    Dim frame, frame_2 As Bitmap
    Public mrX, mrY, mpbX, mpbY As UShort
    Dim MINR, MING, MINB As UShort
    Dim MAXR, MAXG, MAXB As UShort
    Dim DIBUJO As Graphics

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MINR = 0 : MING = 0 : MINB = 0 : MAXR = 255 : MAXG = 255 : MAXB = 255
        mpbX = 0 : mpbY = 0 : iniciar = False : CheckForIllegalCrossThreadCalls = False
        dispositivos = New FilterInfoCollection(FilterCategory.VideoInputDevice)
        For Each X As FilterInfo In dispositivos
            CBListaWebcam.Items.Add(X.Name)
        Next
    End Sub

    Private Sub LWebcam_Click(sender As Object, e As EventArgs) Handles LWebcam.Click
        If iniciar = True Then
            Screen.Close()
            LIluminacion.Enabled = False
            SOV.SignalToStop()
            DIBUJO.Dispose()
            mpbX = 0 : mpbY = 0
            LWebcam.Text = "INICIAR"
            iniciar = False
            LObjeto.Text = "Objeto detectado: NO"
            LAccion.Text = "Acción a ejecutar: NINGUNA"
            LPosX.Text = "Eje X: 0"
            LPosY.Text = "Eje Y: 0"
            LColorEnfocado.Text = "Color enfocado en: NINGUNO"
            PBColor.BackColor = Color.White
            PBWebCam.Image = Nothing : PBWebCamGray.Image = Nothing
            PBWebCam.Image = My.Resources.webcam : PBWebCamGray.Image = My.Resources.webcam
        Else
            If Len(CBListaWebcam.SelectedItem) > 0 Then
                LIluminacion.Enabled = True
                LWebcam.Text = "DETENER"
                iniciar = True
                SOV = New VideoCaptureDevice(dispositivos(CBListaWebcam.SelectedIndex).MonikerString)
                AddHandler SOV.NewFrame, New NewFrameEventHandler(AddressOf Video_NewFrame)
                SOV.Start()
                Screen.Show()
            Else
                MsgBox("Seleccione una WebCam", MsgBoxStyle.Exclamation, "Remote Mouse")
            End If
        End If
    End Sub

    Private Sub Video_NewFrame(sender As Object, eventArgs As AForge.Video.NewFrameEventArgs)
        frame = DirectCast(eventArgs.Frame.Clone(), Bitmap)
        Dim frame_filtrado As Bitmap = DirectCast(eventArgs.Frame.Clone(), Bitmap)
        Dim FILTRO As New ColorFiltering
        FILTRO.Red = New IntRange(MINR, MAXR)
        FILTRO.Green = New IntRange(MING, MAXG)
        FILTRO.Blue = New IntRange(MINB, MAXB)
        FILTRO.ApplyInPlace(frame_filtrado)
        Dim GRIS As Grayscale = Grayscale.CommonAlgorithms.BT709
        Dim IMAGENG As Bitmap = GRIS.Apply(frame_filtrado)
        Dim BLOBS As New BlobCounter()
        BLOBS.MinHeight = 200 : BLOBS.MinWidth = 200
        BLOBS.ObjectsOrder = ObjectsOrder.Size
        BLOBS.ProcessImage(IMAGENG)
        Dim RECTANGULOS As Rectangle() = BLOBS.GetObjectsRectangles()
        If RECTANGULOS.Count = 0 Then
            LAccion.Text = "Acción a ejecutar: NINGUNA"
            LObjeto.Text = "Objeto detectado: NO"
        End If
        If RECTANGULOS.Count > 0 Then
            Dim RECTANGULO As Rectangle = RECTANGULOS(0)
            DIBUJO = Graphics.FromImage(frame)
            Dim TRAZO As New Pen(Color.Lime, 4)
            DIBUJO.DrawRectangle(TRAZO, RECTANGULO)
            mrX = RECTANGULO.Left + (RECTANGULO.Width / 2) : mrY = RECTANGULO.Top + (RECTANGULO.Height / 2)
            LAccion.Text = "Acción a ejecutar: MOVERSE"
            LObjeto.Text = "Objeto detectado: SI"
            LPosX.Text = "Eje X: " & mrX
            LPosY.Text = "Eje Y: " & mrY
        End If
        If mpbX = 0 And mpbY = 0 Then
            LColorEnfocado.Text = "Color enfocado: NINGUNO"
        Else
            If centinelaClick = True Then
                LColorEnfocado.Text = "Color enfocado en: " & mpbX & "," & mpbY
                PBColor.BackColor = Color.FromArgb(frame.GetPixel(mpbX, mpbY).R, frame.GetPixel(mpbX, mpbY).G, frame.GetPixel(mpbX, mpbY).B)
                Call SetRGB(frame.GetPixel(mpbX, mpbY).R, frame.GetPixel(mpbX, mpbY).G, frame.GetPixel(mpbX, mpbY).B)
                centinelaClick = False
            End If
        End If
        PBWebCam.Image = frame : PBWebCamGray.Image = frame_filtrado
    End Sub

    Private Sub PBWebCam_Click(sender As Object, e As MouseEventArgs) Handles PBWebCam.Click
        If iniciar = True Then
            mpbX = Fix((e.X / 6) * 10)
            mpbY = Fix((e.Y / 6) * 10)
            centinelaClick = True
        End If
    End Sub

    Function SetRGB(ByVal r As Integer, ByVal g As Integer, ByVal b As Integer)
        If r - 20 < 0 Then '################Rojo
            TrackBarMINR.Value = 0
        Else
            TrackBarMINR.Value = r - 20
        End If
        If r + 20 > 255 Then
            TrackBarMAXR.Value = 255
        Else
            TrackBarMAXR.Value = r + 20
        End If
        If g - 20 < 0 Then '################Verde
            TrackBarMING.Value = 0
        Else
            TrackBarMING.Value = g - 20
        End If
        If g + 20 > 255 Then
            TrackBarMAXG.Value = 255
        Else
            TrackBarMAXG.Value = g + 20
        End If
        If b - 20 < 0 Then '################Azul
            TrackBarMINB.Value = 0
        Else
            TrackBarMINB.Value = b - 20
        End If
        If b + 20 > 255 Then
            TrackBarMAXB.Value = 255
        Else
            TrackBarMAXB.Value = b + 20
        End If
        Call TrackBarMINR_Scroll(Nothing, Nothing) : Call TrackBarMAXR_Scroll(Nothing, Nothing)
        Call TrackBarMING_Scroll(Nothing, Nothing) : Call TrackBarMAXG_Scroll(Nothing, Nothing)
        Call TrackBarMINB_Scroll(Nothing, Nothing) : Call TrackBarMAXB_Scroll(Nothing, Nothing)
    End Function

    Private Sub TrackBarMINR_Scroll(sender As System.Object, e As System.EventArgs) Handles TrackBarMINR.Scroll
        MINR = TrackBarMINR.Value : LabelMINR.Text = "MinRojo: " & MINR
    End Sub

    Private Sub TrackBarMING_Scroll(sender As System.Object, e As System.EventArgs) Handles TrackBarMING.Scroll
        MING = TrackBarMING.Value : LabelMING.Text = "MinVerde: " & MING
    End Sub

    Private Sub TrackBarMINB_Scroll(sender As System.Object, e As System.EventArgs) Handles TrackBarMINB.Scroll
        MINB = TrackBarMINB.Value : LabelMINB.Text = "MinAzul: " & MINB
    End Sub

    Private Sub TrackBarMAXR_Scroll(sender As System.Object, e As System.EventArgs) Handles TrackBarMAXR.Scroll
        MAXR = TrackBarMAXR.Value : LabelMAXR.Text = "MaxRojo: " & MAXR
    End Sub

    Private Sub TrackBarMAXG_Scroll(sender As System.Object, e As System.EventArgs) Handles TrackBarMAXG.Scroll
        MAXG = TrackBarMAXG.Value : LabelMAXG.Text = "MaxVerde: " & MAXG
    End Sub

    Private Sub TrackBarMAXB_Scroll(sender As System.Object, e As System.EventArgs) Handles TrackBarMAXB.Scroll
        MAXB = TrackBarMAXB.Value : LabelMAXB.Text = "MaxAzul: " & MAXB
    End Sub

    Private Sub LIluminacion_Click(sender As Object, e As EventArgs) Handles LIluminacion.Click
        PBColor.BackColor = Color.White
        Call SetRGB(268, 268, 268)
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If iniciar = True Then
            e.Cancel = True
            MsgBox("Detener la WebCam antes de cerrar", MsgBoxStyle.Exclamation, "Remote Mouse")
        Else
            e.Cancel = False
        End If
    End Sub

End Class

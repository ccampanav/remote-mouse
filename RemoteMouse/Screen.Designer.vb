﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Screen
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Screen))
        Me.LCoorX = New System.Windows.Forms.Label()
        Me.LCoorY = New System.Windows.Forms.Label()
        Me.LTextX = New System.Windows.Forms.Label()
        Me.LTextY = New System.Windows.Forms.Label()
        Me.TimerRepeat = New System.Windows.Forms.Timer(Me.components)
        Me.LCerrar = New System.Windows.Forms.Label()
        Me.PBCursor = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        CType(Me.PBCursor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LCoorX
        '
        Me.LCoorX.AutoSize = True
        Me.LCoorX.BackColor = System.Drawing.Color.Transparent
        Me.LCoorX.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCoorX.Location = New System.Drawing.Point(731, 132)
        Me.LCoorX.Name = "LCoorX"
        Me.LCoorX.Size = New System.Drawing.Size(17, 20)
        Me.LCoorX.TabIndex = 0
        Me.LCoorX.Text = "0"
        Me.LCoorX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LCoorY
        '
        Me.LCoorY.AutoSize = True
        Me.LCoorY.BackColor = System.Drawing.Color.Transparent
        Me.LCoorY.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCoorY.Location = New System.Drawing.Point(729, 158)
        Me.LCoorY.Name = "LCoorY"
        Me.LCoorY.Size = New System.Drawing.Size(17, 20)
        Me.LCoorY.TabIndex = 2
        Me.LCoorY.Text = "0"
        Me.LCoorY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LTextX
        '
        Me.LTextX.AutoSize = True
        Me.LTextX.BackColor = System.Drawing.Color.Transparent
        Me.LTextX.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTextX.Location = New System.Drawing.Point(620, 132)
        Me.LTextX.Name = "LTextX"
        Me.LTextX.Size = New System.Drawing.Size(109, 20)
        Me.LTextX.TabIndex = 3
        Me.LTextX.Text = "Posición en X:"
        Me.LTextX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LTextY
        '
        Me.LTextY.AutoSize = True
        Me.LTextY.BackColor = System.Drawing.Color.Transparent
        Me.LTextY.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTextY.Location = New System.Drawing.Point(620, 158)
        Me.LTextY.Name = "LTextY"
        Me.LTextY.Size = New System.Drawing.Size(107, 20)
        Me.LTextY.TabIndex = 4
        Me.LTextY.Text = "Posición en Y:"
        Me.LTextY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TimerRepeat
        '
        Me.TimerRepeat.Enabled = True
        Me.TimerRepeat.Interval = 5
        '
        'LCerrar
        '
        Me.LCerrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LCerrar.Font = New System.Drawing.Font("Century Gothic", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCerrar.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LCerrar.Location = New System.Drawing.Point(590, 200)
        Me.LCerrar.Name = "LCerrar"
        Me.LCerrar.Size = New System.Drawing.Size(200, 50)
        Me.LCerrar.TabIndex = 5
        Me.LCerrar.Text = "CERRAR"
        Me.LCerrar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PBCursor
        '
        Me.PBCursor.BackColor = System.Drawing.Color.Transparent
        Me.PBCursor.Image = Global.RemoteMouse.My.Resources.Resources.cursor___trans
        Me.PBCursor.Location = New System.Drawing.Point(640, 380)
        Me.PBCursor.Name = "PBCursor"
        Me.PBCursor.Size = New System.Drawing.Size(50, 50)
        Me.PBCursor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBCursor.TabIndex = 6
        Me.PBCursor.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Red
        Me.PictureBox1.Location = New System.Drawing.Point(671, 267)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(40, 40)
        Me.PictureBox1.TabIndex = 7
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Red
        Me.PictureBox2.Location = New System.Drawing.Point(671, 267)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(40, 40)
        Me.PictureBox2.TabIndex = 8
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Red
        Me.PictureBox3.Location = New System.Drawing.Point(671, 267)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(40, 40)
        Me.PictureBox3.TabIndex = 9
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.Red
        Me.PictureBox4.Location = New System.Drawing.Point(671, 267)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(40, 40)
        Me.PictureBox4.TabIndex = 10
        Me.PictureBox4.TabStop = False
        '
        'Screen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1378, 780)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PBCursor)
        Me.Controls.Add(Me.LCerrar)
        Me.Controls.Add(Me.LTextY)
        Me.Controls.Add(Me.LTextX)
        Me.Controls.Add(Me.LCoorY)
        Me.Controls.Add(Me.LCoorX)
        Me.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximizeBox = False
        Me.Name = "Screen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Remote cursor"
        CType(Me.PBCursor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LCoorX As Label
    Friend WithEvents LCoorY As Label
    Friend WithEvents LTextX As Label
    Friend WithEvents LTextY As Label
    Friend WithEvents TimerRepeat As Timer
    Friend WithEvents LCerrar As Label
    Friend WithEvents PBCursor As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
End Class

﻿Public Class Screen
    Dim desktopSize As Size
    Dim dskX, dskY, wcX, wcY As UShort
    Dim toleX, toleY, recX, recY As UShort
    Dim beforeX, beforeY, toleMov As UShort
    Dim incX, incY As Long

    Private Sub Screen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        wcX = 640 : wcY = 480 'Resolución de la webcam
        desktopSize = System.Windows.Forms.SystemInformation.PrimaryMonitorSize 'Obtener información del monitor
        dskX = desktopSize.Width : dskY = desktopSize.Height 'Establecer largo y ancho del monidot
        Me.Width = dskX : Me.Height = dskY 'Ajustar tamaño del formulario al del monitor
        PictureBox1.Left = 0 : PictureBox1.Top = 0
        PictureBox2.Left = dskX - 40 : PictureBox2.Top = 0
        PictureBox3.Left = 0 : PictureBox3.Top = dskY - 40
        PictureBox4.Left = dskX - 40 : PictureBox4.Top = dskY - 40
        toleX = 0 : toleY = 0 'Tolerancia en cada borde
        recX = wcX - (toleX * 2) : recY = wcY - (toleY * 2) 'Tamaños del nuevo rectangulo con tolerancia
        incX = dskX / recX : incY = dskY / recY 'Incremento para el tamaño del monitor
        toleMov = 4 'Tolerancia de movimientos involuntarios
        beforeX = 640 : beforeY = 380 'Movimientos en 0

    End Sub

    Private Sub TimerRepeat_Tick(sender As Object, e As EventArgs) Handles TimerRepeat.Tick
        LCoorX.Text = Form1.mrX
        LCoorY.Text = Form1.mrY
        If LCoorX.Text > beforeX Then 'Omite movimientos involuntarios en X
            If LCoorX.Text - beforeX >= toleMov Then
                PBCursor.Left = dskX - Fix(LCoorX.Text * incX)
            End If
        Else
            If beforeX - LCoorX.Text >= toleMov Then
                PBCursor.Left = dskX - Fix(LCoorX.Text * incX)
            End If
        End If
        If LCoorY.Text > beforeY Then 'Omite movimientos involuntarios en Y
            If LCoorY.Text - beforeY >= toleMov Then
                PBCursor.Top = Fix(LCoorY.Text * incY)
            End If
        Else
            If beforeY - LCoorY.Text >= toleMov Then
                PBCursor.Top = Fix(LCoorY.Text * incY)
            End If
        End If
        beforeX = LCoorX.Text
        beforeY = LCoorY.Text
    End Sub

    Private Sub LCerrar_Click(sender As Object, e As EventArgs) Handles LCerrar.Click
        Me.Close()
    End Sub

    Private Sub Screen_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        TimerRepeat.Enabled = False
    End Sub

End Class